import React from "react";
import Item from "./Item";
import { useCartContext } from "../context/Context";
import "../components/Cart.css";

const Cart = () => {
  const { product, total, amount, formatMoney } = useCartContext();
  // console.log("สินค้าทั้งหมด", product);
  // console.log("จำนวนรวมที่จะซื้อ", total);
  // console.log("จำนวนสินค้า", amount);
  return (
    <div className="carts">
      <h1 className="cart-total" style={{ textAlign: "center" }}>
        {product.length > 0
          ? `ยอดที่ต้องชำระเงินรวม : ${formatMoney(total)}`
          : "ไม่มีสินค้า"}
      </h1>
      {product.map((data) => (
        <Item key={data.id} data={data} />
      ))}
    </div>
  );
};

export default Cart;
