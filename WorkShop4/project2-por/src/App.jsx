import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import Home from './component/Home'
import Blogs from './component/Blogs'
import About from './component/About'
import {BrowserRouter, Routes,Route,Navigate} from "react-router-dom"
import Navbar from './component/Navbar'
import NotFound from './component/NotFound'
import Details from './component/Details'
function App() {
  

  return (
    <BrowserRouter>
    <Navbar/>
        <Routes>
            <Route path="/" element={<Home/>}></Route>
            <Route path="/about" element={<About/>}></Route>
            <Route path="/blogs" element={<Blogs/>}></Route>
            <Route path="*" element={<NotFound/>}></Route>
            <Route path="/home" element={<Navigate to="/"/>} ></Route>
            <Route path="/info" element={<Navigate to ="/about"/>}></Route>
            <Route path="/blog/:id" element={<Details/>}></Route>
        </Routes>
    </BrowserRouter>
  );
}

export default App
