const blogs=[
    {
        id:1,
        title:"ต้มยำทะเล",
        image_url:"https://cms.dmpcdn.com/food/2022/08/08/19cefee0-16d3-11ed-bf11-e351fbb7ed22_webp_original.jpg",
        content:"Lorem ipsum dolor sit amet consectetur adipisicing elit. Distinctio voluptatibus consequuntur nisi velit cum veritatis pariatur est aliquid cumque vel facere et ducimus reiciendis quibusdam quaerat alias dolorem, vero doloribus.",
        author:"ดวงหทัย"
    },
    {
        id:2,
        title:"ข้าวคลุกกะปิ",
        image_url:"https://cms.dmpcdn.com/food/2020/06/10/a3e58e90-aae3-11ea-8ff7-3155576e8b02_original.jpg",
        content:"Lorem ipsum dolor sit amet consectetur adipisicing elit. Distinctio voluptatibus consequuntur nisi velit cum veritatis pariatur est aliquid cumque vel facere et ducimus reiciendis quibusdam quaerat alias dolorem, vero doloribus.",
        author:"ดวงหทัย"

    },
    {
        id:3,
        title:"ปลาดุกผัดพริกขิง",
        image_url:"https://cms.dmpcdn.com/food/2021/04/28/c2344f10-a7fb-11eb-82d0-f787c3df094f_original.jpg",
        content:"Lorem ipsum dolor sit amet consectetur adipisicing elit. Distinctio voluptatibus consequuntur nisi velit cum veritatis pariatur est aliquid cumque vel facere et ducimus reiciendis quibusdam quaerat alias dolorem, vero doloribus.",
        author:"ดวงหทัย"

    },
    {
        id:4,
        title:"กุ้งทอดเกลือ",
        image_url:"https://cms.dmpcdn.com/food/2022/07/07/8038caf0-fde0-11ec-b5e0-5f8883ae8101_original.jpg",
        content:"Lorem ipsum dolor sit amet consectetur adipisicing elit. Distinctio voluptatibus consequuntur nisi velit cum veritatis pariatur est aliquid cumque vel facere et ducimus reiciendis quibusdam quaerat alias dolorem, vero doloribus.",
        author:"ดวงหทัย"

    },
    {
        id:5,
        title:"ไข่ชะอม",
        image_url:"https://cms.dmpcdn.com/food/2022/07/07/75af66a0-fdb5-11ec-b6fe-094765bdf802_webp_original.jpg",
        content:"Lorem ipsum dolor sit amet consectetur adipisicing elit. Distinctio voluptatibus consequuntur nisi velit cum veritatis pariatur est aliquid cumque vel facere et ducimus reiciendis quibusdam quaerat alias dolorem, vero doloribus.",
        author:"ดวงหทัย"

    }
]
export default blogs;