import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import "./Content.css";

export default function DataList() {
  return (
    <div>
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          paddingLeft: "2%",
          paddingRight: "2%",
          fontSize: "12vh",
        }}
      >
        <Typography
          sx={{ fontSize: "20px", fontWeight: "bold", margin: "18px" }}
        >
          Subscriptons
        </Typography>

        <Typography
          sx={{
            background: "#212121",
            borderRadius: "50%",
            height: "22px",
            width: "22px",
            color: "#fff",
            display: "flex",
            justifyContent: "center",
            fontWeight: 600,
          }}
        >
          2
        </Typography>
      </Box>

      <Box
        sx={{
          borderRadius: "1.5rem",
          backgroundColor: "#E0DDDD",
          //   marginLeft: "1%",
          //   paddingLeft: "1%",
          //   marginRight: "1%",
          //   paddingRight: "1%",
          padding: "1%",
          margin: "1%",
          display: "flex",
          alignItems: "center",
          maxHeight: "90px",
        }}
      >
        <Box
          sx={{
            backgroundColor: "white",
            width: "10%",
            alignItems: "center",
            borderRadius: "1.5rem",
            display: "flex",
            maxHeight: "80px",
            maxWidth: "80px",
          }}
        >
          <img
            src="./github.png"
            style={{
              width: "90%",
              height: "90%",
              display: "flex",
              maxHeight: "90px",
              maxWidth: "90px",
              position: "relative",
              left: "50%",
              transform: "translateX(-50%)",
            }}
          ></img>
        </Box>
        <Box
          sx={{ marginLeft: "2%", backgroundColor: "#E0DDDD", width: "40%" }}
        >
          <Typography
            sx={{ fontSize: "2vh", fontWeight: "bold", marginLeft: "2%" }}
          >
            Github
          </Typography>
          <Typography
            sx={{ fontSize: "1.5vh", fontWeight: "100", marginLeft: "2%" }}
          >
            Monthly
          </Typography>
        </Box>

        <Typography
          sx={{
            fontSize: "1rem",
            fontWeight: "bold",
            marginLeft: "25%",
            marginRight: "15px",
          }}
        >
          -$29.99
        </Typography>
      </Box>
      <Box
        sx={{
          borderRadius: "1.5rem",
          backgroundColor: "#E0DDDD",
          //   marginLeft: "1%",
          //   paddingLeft: "1%",
          //   marginRight: "1%",
          //   paddingRight: "1%",
          padding: "1%",
          margin: "1%",
          display: "flex",
          alignItems: "center",
          maxHeight: "90px",
        }}
      >
        <Box
          sx={{
            backgroundColor: "white",
            width: "10%",
            alignItems: "center",
            borderRadius: "1.5rem",
            display: "flex",
            maxHeight: "80px",
            maxWidth: "80px",
          }}
        >
          <img
            src="./spotify.png"
            style={{
              width: "90%",
              height: "90%",
              display: "flex",
              maxHeight: "80px",
              maxWidth: "80px",
              position: "relative",
              left: "50%",
              transform: "translateX(-50%)",
            }}
          ></img>
        </Box>
        <Box
          sx={{ marginLeft: "2%", backgroundColor: "#E0DDDD", width: "40%" }}
        >
          <Typography
            sx={{ fontSize: "2vh", fontWeight: "bold", marginLeft: "2%" }}
          >
            Spotify
          </Typography>
          <Typography
            sx={{ fontSize: "1.5vh", fontWeight: "light", marginLeft: "2%" }}
          >
            Annual
          </Typography>
        </Box>

        <Typography
          sx={{
            fontSize: "1rem",
            fontWeight: "bold",
            marginLeft: "25%",
            marginRight: "15px",
          }}
        >
          -$9.99
        </Typography>
      </Box>
    </div>
  );
}
