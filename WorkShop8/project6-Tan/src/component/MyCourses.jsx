import React, { useState } from "react";
import LinearProgress from "@mui/material/LinearProgress";
import "./MyCourses.css";

import { alpha } from "@mui/material/styles";
import Box from "@mui/material/Box";
import InputBase from "@mui/material/InputBase";
import SearchIcon from "@mui/icons-material/Search";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import { Avatar, CardActionArea, IconButton } from "@mui/material";
import CssBaseline from "@mui/material/CssBaseline";
import BottomNavigation from "@mui/material/BottomNavigation";
import BottomNavigationAction from "@mui/material/BottomNavigationAction";
import Paper from "@mui/material/Paper";
import WaterDropIcon from "@mui/icons-material/WaterDrop";
import { styled } from "@mui/material/styles";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";

import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import StarOutlineIcon from "@mui/icons-material/StarOutline";
import BookmarkBorderIcon from "@mui/icons-material/BookmarkBorder";
import "react-multi-carousel/lib/styles.css";
import BookIcon from "@mui/icons-material/Book";
import TopicIcon from "@mui/icons-material/Topic";
import AccessTimeIcon from "@mui/icons-material/AccessTime";

import DomainVerificationIcon from "@mui/icons-material/DomainVerification";
import Button from "@mui/material/Button";
import HomeIcon from "@mui/icons-material/Home";
import MenuBookIcon from "@mui/icons-material/MenuBook";
import NotificationAddIcon from "@mui/icons-material/NotificationAdd";
import PermIdentityIcon from "@mui/icons-material/PermIdentity";

const Search = styled("div")(({ theme }) => ({
  position: "relative",
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  "&:hover": {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginLeft: 0,
  width: "100%",
  [theme.breakpoints.up("sm")]: {
    marginLeft: theme.spacing(1),
    width: "auto",
  },
}));
const SearchIconWrapper = styled("div")(({ theme }) => ({
  padding: theme.spacing(0, 20),
  height: "100%",
  width: "50%",
  position: "absolute",
  pointerEvents: "none",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: "inherit",
  "& .MuiInputBase-input": {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      width: "12ch",
      "&:focus": {
        width: "20ch",
      },
    },
  },
}));

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));

function MyChips() {
  const [selected, setSelected] = useState("All Topic");

  const handleChipClick = (value) => {
    setSelected(value);
  };

  const handleChipKeyDown = (event, value) => {
    if (event.key === "Enter") {
      setSelected(value);
    }
  };
}

const StyledToolbar = styled(Toolbar)(({ theme }) => ({
  paddingTop: theme.spacing(1),
  paddingBottom: theme.spacing(2),
  // Override media queries injected by theme.mixins.toolbar
  "@media all": {
    width: "100%",
    display: "flex",
    justifyContent:"space-between",
    backgroundColor: "#fff",
    color: "#000",
  },
}));

const progress = 50;

export default function MyCourses() {
  const [alignment, setAlignment] = React.useState("web");

  const handleChange = (event, newAlignment) => {
    setAlignment(newAlignment);
  };

  return (
    <Grid>
      <div className="container">
        <React.Fragment>
          <CssBaseline />

          <Container maxWidth="lg" minWidth="380%">
            <Box sx={{ bgcolor: "#cfe8fc" }}>
              <Box display="flex" alignItems="center">
                <Box sx={{ flexGrow: 1 }}>
                  <AppBar position="static">
                    <StyledToolbar>
                      <Box>
                        <IconButton
                          size="large"
                          edge="start"
                          color="inherit"
                          aria-label="open drawer"
                        >
                          <ArrowBackIcon />
                        </IconButton>
                      </Box>
                      <Box>
                        <h3>My Courses</h3>
                      </Box>
                      <Box>
                        <IconButton
                          size="large"
                          aria-label="search"
                          color="inherit"
                        >
                          <SearchIcon />
                        </IconButton>
                      </Box>
                    </StyledToolbar>
                  </AppBar>
                </Box>
              </Box>

              <Box className="Courses">
                <Grid container alignItems="center" item xs={12} >
                  <Box sx={{ width: "100%", m:1 }}>
                    <Grid
                      container
                      Spacing={1}
                      columnSpacing={{ xs: 2, sm: 1, md: 4 }}
                    >
                      <Grid item xs={2}  m={2} display="flex" justifyContent="center" rowSpacing={2}  >
                        <Item value="All Topic" sx={{ borderRadius: "20px" }}>
                          <Box display="flex" alignItems="center">
                            <Box display="flex">
                              <Box
                                sx={{
                                  p: 0.5,
                                  bgcolor: "#fff",
                                  borderRadius: "50%",
                                }}
                              >
                                <IconButton color="primary">
                                  <BookIcon />
                                </IconButton>
                              </Box>
                            </Box>

                            <Box sx={{ ml: 1 }}>
                              <Typography>All</Typography>
                            </Box>
                          </Box>
                        </Item>
                      </Grid>

                      <Grid item xs={3}    m={2}   display="flex" justifyContent="center" rowSpacing={2}  >
                        <Item sx={{ borderRadius: "20px" }}>
                          <Box display="flex" alignItems="center">
                            <Box display="flex">
                              <Box
                                sx={{
                                  p: 0.5,
                                  bgcolor: "#FFD700",
                                  borderRadius: "50%",
                                }}
                              >
                                <IconButton>
                                  <WaterDropIcon sx={{ color: "#ffff" }} />
                                </IconButton>
                              </Box>
                            </Box>
                            <Box sx={{ ml: 1 }}>
                              <Typography>Ongoing</Typography>
                            </Box>
                          </Box>
                        </Item> 
                      </Grid>

                      <Grid item xs={3}   m={2}  display="flex" justifyContent="center" rowSpacing={2}   >
                        <Item sx={{ borderRadius: "20px" }}>
                          <Box display="flex" alignItems="center">
                            <Box display="flex">
                              <Box
                                sx={{
                                  p: 0.5,
                                  bgcolor: "#6A5ACD",
                                  borderRadius: "50%",
                                }}
                              >
                                <IconButton>
                                  <DomainVerificationIcon
                                    sx={{ color: "#ffff" }}
                                  />
                                </IconButton>
                              </Box>
                            </Box>
                            <Box sx={{ ml: 1 }}>
                              <Typography>Complete</Typography>
                            </Box>
                          </Box>
                        </Item>
                      </Grid>
                      
                    </Grid>
                  </Box>
                </Grid>
              </Box>

              <br></br>
              
              <React.Fragment>
                <div className="item-check">
                  <Grid item xs={12} sm={6} md={4}>
                    <Card
                      sx={{
                        height: 270,
                        minHeight: 80,
                        m: 2,
                        p: 5,
                        borderRadius: "10%",
                        display: "flex",
                        backgroundColor: "#87CEFA",
                      }}
                    >
                      <CardContent
                        style={{ display: "flex", alignItems: "center" }}
                      >
                        
                        <Typography variant="body2" color="text.secondary">
                          <h3>Find acourse you</h3>
                          <h3>want to learn !</h3>
                          <br></br>
                          <p>
                            Lizards are a widespread group of squamate reptiles,
                            with over ranging across all continents except
                            Antarctica
                          </p>
                          <br></br>
                          <div>
                            <Button
                              variant="contained"
                              display="flex"
                              sx={{
                                borderRadius: "10%",
                                bgcolor: "#fff",
                                color: "#000",
                              }}
                            >
                              Check Now
                            </Button>
                          </div>
                        </Typography>
                      
                        <div >
                          <img   src="https://img.freepik.com/free-vector/flat-business-employees-meeting-teamwork-training-teaching_88138-990.jpg?w=1380&t=st=1683777681~exp=1683778281~hmac=221d11e7a57e51ee8e49c0fe48d68caf59db49d501231a3f3d1f8ff816d63f83"></img>
                        </div>
                      </CardContent>
                    </Card>
                  </Grid>
                </div>
                <br></br>
                <div className="item1">
                  <Grid item xs={12} sm={6} md={5} display="flex">
                    <Card
                      sx={{
                        height: 270,
                        minHeight: 60,
                        m: 2,
                        p:1,
                        
                        borderRadius: "10%",
                        display: "flex",
                        backgroundColor: "#6A5ACD",
                      }}
                    >
                      <CardContent
                        style={{ display: "flex", alignItems: "center" }}
                      >
                        <img src="https://img.freepik.com/free-vector/freelancer-working-laptop-her-house_1150-35054.jpg?w=740&t=st=1683611929~exp=1683612529~hmac=d20a31f2c85eef8714f53cab18429262f5ba408e39b36df8871ebccc0a4d84d6"></img>

                        <Typography variant="body2" color="text.secondary"  >
                          <br></br>
                          <br></br>
                          <br></br>
                          
                          
                          <h3>Digital Design Thinking</h3>
                          
                          <p>
                            Lizards are a widespread group of squamate reptiles,
                
                            
                          </p>
                          <IconButton color="primary">
                            <TopicIcon />
                          </IconButton>
                          17: Filed
                          <IconButton color="primary" sx={{ m: 1 }}>
                            <AccessTimeIcon />
                          </IconButton>
                          40 : Min
                          <div>
                            Complete 70%
                            <LinearProgress
                              variant="determinate"
                              value={progress}
                              backgroundColor="#B0C4DE"
                            />
                          </div>
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                </div>
                <div className="item2">
                  <Grid item xs={12} sm={6} md={4}>
                    <Card
                      sx={{
                        height: 270,
                        minHeight: 60,
                        m: 2,
                        p: 0.5,
                        borderRadius: "10%",
                        display: "flex",
                        backgroundColor: "#7FFFD4",
                      }}
                    >
                      <CardContent
                        style={{ display: "flex", alignItems: "center" }}
                      >
                        <img src="https://img.freepik.com/premium-vector/vector-concept-illustration-programmer-engineer-with-laptop-sitting-office-desk-holding-pen-while-coding-developing-flat-cartoon-style_270158-379.jpg?w=1060"></img>

                        <Typography variant="body2" color="text.secondary">
                          <br></br>
                          <br></br>
                          <br></br>
                          
                          
                          <h3>Web Development</h3>
                          
                         
                          <p>
                            Lizards are a widespread group of squamate reptiles,
                            
                          
                          </p>
                          <br></br>
                          <IconButton color="primary">
                            <TopicIcon />
                          </IconButton>
                          17: Filed
                          <IconButton color="primary" sx={{ m: 1 }}>
                            <AccessTimeIcon />
                          </IconButton>
                          40 : Min
                          <div>
                            Complete 80%
                            <LinearProgress
                              variant="determinate"
                              value={progress}
                              backgroundColor="#B0C4DE"
                            />
                          </div>
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                </div>
                <div className="item3">
                  <Grid item xs={12} sm={6} md={4}>
                    <Card
                      sx={{
                        height: 270,
                        minHeight: 60,
                        m: 2,
                        p: 1,
                        borderRadius: "10%",
                        display: "flex",
                        backgroundColor: "#D2691E",
                      }}
                    >
                      <CardContent 
                        style={{ display: "flex", alignItems: "center" }}
                      >
                        <img src="https://img.freepik.com/free-vector/computer-programming-camp-abstract-concept-illustration_335657-3921.jpg?w=740&t=st=1683687566~exp=1683688166~hmac=d6c1071c5b6bc173863b90ad9ddb0cc27525553b94d342e2847adf74ecdc4ee4"></img>

                        <Typography variant="body2" color="text.secondary">
                        <br></br>
                          <br></br>
                          <br></br>
                          <br></br>
                          
                          <h3> Design Thinking</h3>
                          
                          <p>
                            Lizards are a widespread group of squamate reptiles,
                            
                            
                          </p>
                          <IconButton color="primary">
                            <TopicIcon />
                          </IconButton>
                          17: Filed
                          <IconButton color="primary" sx={{ m: 1 }}>
                            <AccessTimeIcon />
                          </IconButton>
                          40 : Min
                          <div>
                            Complete 60%
                            <LinearProgress
                              variant="determinate"
                              value={progress}
                              backgroundColor="#B0C4DE"
                            />
                          </div>
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                </div>
              </React.Fragment>
              <br></br>
              <br></br>
            </Box>
          </Container>

          <Box>
            <Paper
              sx={{ position: "fixed", bottom: 0, left: 0, right: 30 }}
              elevation={2}
            >
              <BottomNavigation>
                <BottomNavigationAction label="Home" icon={<HomeIcon />} />
                <BottomNavigationAction
                  label="star"
                  icon={<StarOutlineIcon />}
                />

                <BottomNavigationAction label="menu" icon={<MenuBookIcon />} />
                <BottomNavigationAction
                  label="Notifi"
                  icon={<NotificationAddIcon />}
                />
                <BottomNavigationAction
                  label="person"
                  icon={<PermIdentityIcon />}
                />
              </BottomNavigation>
            </Paper>
          </Box>
        </React.Fragment>
      </div>
    </Grid>
  );
}
