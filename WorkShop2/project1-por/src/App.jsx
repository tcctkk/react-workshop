import { useState } from "react";
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import Header from './Components/Header';
import StudentList from './Components/StudentList';
import './App.css';
import AddForm from "./Components/addForm";


function App() {
  const [students,setStudent] = useState([
    {id:1, name:" ปอ"}
  ]);
  function deleteStudent(id){
    setStudent(students.filter((item) =>item.id !== id));
  }

  return (
    <div className='App'>
      <Header title = "HOME"/>
      <main>
        <AddForm students={students} setStudent={setStudent}/>
        <StudentList students= {students} deleteStudent={deleteStudent}/>
      </main>
    </div>
  )
}

export default App;
