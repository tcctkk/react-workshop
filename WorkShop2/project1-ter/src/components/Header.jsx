import './Header.css'
import { BiSun } from "react-icons/bi";
import { BiMoon } from "react-icons/bi";
export default function Header(props){
    const {theme,setTheme} = props
    function ToggleTheme(){
        if(theme === "light"){
            setTheme("dark")
        }else{
            setTheme("light")
        }
    }
    return(
        <nav>
            <div className="header">
                <span>Project1</span>
            </div>
            <ul className="link">
                <li><a href="/">Home</a></li>
                <li><a href="/">Contact</a></li>
            </ul>
            <div className="theme" >
                <span className="icon" onClick={ToggleTheme}>
                {theme === "light" ? <BiSun /> : <BiMoon/>}
                </span>
            </div>
        </nav>
    )
}