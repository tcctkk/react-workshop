import NotfoundImg from "../img/Notfound.jpg";
export default function Notfound(){
    return(
    <>
    <div className="container">
    <h2 className="title">ไม่พบหน้าเว็บ (404 Page Not Found)</h2>
    <img src={NotfoundImg} alt="NotfoundImg"/>
    </div>
    </>
    );
}