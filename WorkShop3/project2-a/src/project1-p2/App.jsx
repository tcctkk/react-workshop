import { useState, useEffect } from "react";
import "./App.css";
import AddForm from "./AddForm";
import Item from "./Item";
import Header from "./Header";
function App() {
  const [tasks, setTasks] = useState(
    JSON.parse(localStorage.getItem("tasks")) || []
  );
  const [title, setTitle] = useState("");
  const [editID, setEditID] = useState(null);
  const [theme, setTheme] = useState("light");

  useEffect(() => {
    // console.log("UseEffect");
    localStorage.setItem("tasks", JSON.stringify(tasks));
  }, [tasks]);

  const deleteTasks = (id) => {
    try {
      console.log("ID ที่ลบออกคือ : ", id);
      setTasks(tasks.filter((data) => data.id !== id));
    } catch (err) {
      console.log("Error ", err);
    }
  };

  const saveTask = (events) => {
    events.preventDefault();
    try {
      if (!title) {
        alert("Please input name");
      } else if (editID) {
        //อัพเดทข้อมูล
        console.log("ID ที่จะมีการอัพเดท", editID);
        //รายการที่ตรงกับ id
        const updateTask = tasks.map((data) => {
          if (data.id === editID) {
            const newData = { ...data, title: title };
            console.log(`ข้อมูลของ tasks ${data.id} ที่มีการ update`, newData);
            return newData;
          }
          return data;
        });
        console.log("ข้อมูลใน tasks ทั้งหมดที่มีการ update", updateTask);
        setTasks(updateTask);
        setTitle("");
        setEditID("");
        // setTasks([...tasks, newTask]);
      } else {
        //เพิ่มรายการใหม่
        let newTask = {
          id: Math.floor(Math.random() * 3) + 1,
          title: title,
        };
        console.log("ID : ", newTask.id);
        console.log("task ใหม่ : ", newTask.title);

        while (tasks.find((task) => task.id === newTask.id)) {
          newTask = {
            ...newTask,
            id: Math.floor(Math.random() * 1000) + 1,
          };
          console.log("newTask ID : ", newTask.id);
        }
        setTasks([...tasks, newTask]);
        setTitle("");
        console.log("tasks ใหม่ที่มีการเพิ่มข้อมูลแล้ว : ", tasks);
      }
    } catch (err) {
      console.log("Error ", err);
    }
  };

  const editTasks = (id) => {
    console.log("รับ ID ที่มาจากการกดปุ่มแก้ไข : ", id);
    setEditID(id);
    const editTask = tasks.find((data) => data.id === id);
    setTitle(editTask.title);
  };
  try {
    return (
      <div className={"App " + theme}>
        <Header theme={theme} setTheme={setTheme} />
        <div className="container">
          <AddForm
            title={title}
            setTitle={setTitle}
            saveTask={saveTask}
            editID={editID}
          />
          <section>
            {tasks.map((data) => (
              <Item
                key={data.id}
                data={data}
                deleteTasks={deleteTasks}
                editTasks={editTasks}
              />
            ))}
          </section>
        </div>
      </div>
    );
  } catch (err) {
    console.log("Error", err);
  }
}

export default App;
