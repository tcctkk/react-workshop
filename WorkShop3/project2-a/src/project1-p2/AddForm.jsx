import React from "react";
import "./AddForm.css";

const AddForm = ({ title, setTitle, saveTask, editID }) => {
  return (
    <div>
      <h2>แอพบริหารจัดการงาน</h2>
      <form onSubmit={saveTask}>
        <div className="form-control">
          <input
            type="text"
            className="text-input"
            name="title"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
          />
          <button type="submit" className="submit-btn">
            {editID || editID === 0 ? "อัพเดท" : "เพิ่ม"}
          </button>
        </div>
      </form>
    </div>
  );
};

export default AddForm;
