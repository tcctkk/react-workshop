const blogs=[
    {
        id:1,
        title:"แรก",
        image_url:"https://img.freepik.com/free-photo/cute-ai-generated-cartoon-bunny_23-2150288868.jpg?w=826&t=st=1683001926~exp=1683002526~hmac=dbd1b69175456213efc4abd6209b2e0ef1240e17772b1f1bae3b8320933f5aa9",
        content:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum",
        author:"Ter"
    },
    {
        id:2,
        title:"สอง",
        image_url:"https://img.freepik.com/premium-photo/kawaii-gamer-set-videoconsole-gaming-pc-computer-game-controller-illustration_691560-5688.jpg?w=826",
        content:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum",
        author:"Ter"
    },
    {
        id:3,
        title:"สาม",
        image_url:"https://img.freepik.com/free-photo/cute-ai-generated-cartoon-bunny_23-2150288868.jpg?w=826&t=st=1683001926~exp=1683002526~hmac=dbd1b69175456213efc4abd6209b2e0ef1240e17772b1f1bae3b8320933f5aa9",
        content:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum",
        author:"Ter"
    },
    {
        id:4,
        title:"สี่",
        image_url:"https://img.freepik.com/premium-photo/kawaii-gamer-set-videoconsole-gaming-pc-computer-game-controller-illustration_691560-5688.jpg?w=826",
        content:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum",
        author:"อู้วววววว"
    },
]
export default blogs;