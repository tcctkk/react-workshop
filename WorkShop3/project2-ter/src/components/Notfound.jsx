import "./Notfound.css";
export default function Notfound() {
  return (
    <>
      <div className="container">
        <div className="title">
          <h3>ไม่พบหน้า (404 Page Not Found)</h3>
        </div>
        <img
          src="https://img.freepik.com/free-vector/error-404-concept-landing-page_52683-18756.jpg?w=826&t=st=1682999997~exp=1683000597~hmac=2b62f7a3d2add5110231f1472a2195adc271000e75b8b388c89ec1626c19a6d4"
          alt=""
        />
      </div>
    </>
  );
}
