import { useState, useEffect } from "react";
import blogs from "../data/blogs";
import "./Blog.css";
import { Link } from "react-router-dom";
export default function Blogs() {
    const [search,setSearch]=useState("");
    const [filterBlog,setFilterblog] = useState([]);
    useEffect(()=>{
        const result = blogs.filter((item)=>item.title.includes(search))
        setFilterblog(result)
        console.log(result);
    },[search])
  return (
    <>
      <div className="container">
        <div className="title">
          <h2>บทความทั้งหมด</h2>
        </div>
        <div className="search">
            <input type="text" 
            className="search-input"
            placeholder="ค้นหา"
            value={search}
            onChange={(e) => setSearch(e.target.value)} />
        </div>
      </div>
      
      <div className="blogs-container">
        <article>
          {filterBlog.map((item) => (
            <Link to={`/blog/${item.id}`} key={item.id} className="blogcard">
              <div className="card">
                <h2 className="titletext">{item.title}</h2>
                <p>{item.content.substring(0, 200)}</p>
                <hr />
              </div>
            </Link>
          ))}
        </article>
      </div>
    </>
  );
}
