import React, { useState, useEffect } from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Grid from "@mui/material/Grid";
import {
  styled,
  Paper,
  Box,
  Typography,
  Link,
  Button,
  Avatar,
} from "@mui/material";
import Container from "@mui/material/Container";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";

export default function UsersCard() {
  const [items, setItem] = useState([]);

  const userGet = () => {
    fetch("https://www.melivecode.com/api/users/")
      .then((response) => response.json())
      .then((result) => {
        setItem(result);
      });
  };
  //use when delete user, it'll reload page w/o deleted user
  useEffect(() => {
    userGet();
  }, []);

  const updateUser = (id) => {
    window.location = "/update/" + id;
  };

  const deleteUser = (id) => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      id: id,
    });
    var requestOptions = {
      method: "DELETE",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch("https://www.melivecode.com/api/users/delete", requestOptions)
      .then((response) => response.json())
      .then((result) => {
        if (result["status"] === "ok") {
          userGet();
        }
        console.log(result);
      })
      .catch((error) => console.log("error", error));
  };

  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="lg" sx={{ p: 2 }}>
        <Paper sx={{ p: 2 }}>
          <Box display={"flex"}>
            <Box sx={{ flexGrow: 1, pb: 1 }}>
              <Typography variant="h6" gutterBottom component="div">
                Users
              </Typography>
            </Box>
            <Box sx={{ pb: 2 }}>
              <Link href="create">
                <Button variant="contained">Create</Button>
              </Link>
            </Box>
          </Box>
          <Box
            sx={{
              display: "flex",
              flexWrap: "wrap",
              justifyContent: "center",
            }}
          >
            {items.map((card) => (
              <Grid
                item
                xs={2}
                sm={3}
                md={3}
                key={card.id}
                sx={{
                  display: "inline-grid",
                }}
              >
                <Card
                  sx={{
                    mb: 1,
                    mr: 1.5,
                    display: "inline-grid",
                    width: 300,
                  }}
                >
                  <CardContent
                    sx={{
                      px: "auto",
                    }}
                  >
                    <Typography
                      gutterBottom
                      component="div"
                      color="text.secondary"
                      sx={{
                        fontSize: "0.85rem",
                        display: "flex",
                        justifyContent: "flex-end",
                      }}
                    >
                      User ID: {card.id}
                    </Typography>
                    <Avatar
                      src={card.avatar}
                      alt={card.fname}
                      sx={{ width: 70, height: 70, mb: 2 }}
                    />
                    <Box>
                      <Typography sx={{ fontSize: "1rem" }}>
                        Name: {card.fname} {card.lname}
                      </Typography>
                    </Box>
                    <Box sx={{ height: 24 }}>
                      <Typography sx={{ fontSize: "0.85rem" }}>
                        Username:{card.username}
                      </Typography>
                    </Box>
                  </CardContent>
                  <CardActions
                    sx={{ display: "flex", justifyContent: "flex-end", pr: 2 }}
                  >
                    <Button
                      onClick={() => updateUser(card.id)}
                      size="small"
                      variant="outlined"
                    >
                      Edit
                    </Button>
                    <Button
                      onClick={() => deleteUser(card.id)}
                      size="small"
                      variant="outlined"
                    >
                      Delete
                    </Button>
                  </CardActions>
                </Card>
              </Grid>
            ))}
          </Box>
        </Paper>
      </Container>
    </React.Fragment>
  );
}
