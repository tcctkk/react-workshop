import * as React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import { CardActionArea, Grid } from "@mui/material";
import CssBaseline from "@mui/material/CssBaseline";
import Container from "@mui/material/Container";
import { useState, useEffect } from "react";
export default function Cards() {
  const [items, setItems] = useState([]);
  useEffect(() => {
    UserGet();
  }, []);

  const UserGet = () => {
    fetch("https://4ef6-202-28-119-38.ngrok-free.app/customer")
      .then((res) => res.json())
      .then((result) => {
        setItems(result);
      });
  };
  return (
    <>
      <React.Fragment>
        <CssBaseline />
        <Container maxWidth="lg">
          <h1>Cards</h1>
          <Grid
            container
            spacing={2}
            sx={{
              display: "flex",
              textAlign: "center",
              justifyContent: "center",
            }}
          >
            {items.map((item) => (
              <Card
                sx={{ maxWidth: 190, m: 1, minWidth: 190}}
                key={item.customer_id}
                xs={12}
                sm={6}
              >
                <CardActionArea sx={{minHeight:150}}>
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                      {item.customer_name}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                      Phone: {item.phone_number}
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            ))}
          </Grid>
        </Container>
      </React.Fragment>
    </>
  );
}
