import { useState } from "react";
import "./calculate.css";

function Calculator() {
  const [num1, setNum1] = useState("");
  const [num2, setNum2] = useState("");
  const [operator, setOperator] = useState("+");

  const handleNum1Change = (event) => {
    setNum1(event.target.value);
  };

  const handleNum2Change = (event) => {
    setNum2(event.target.value);
  };

  const handleOperatorChange = (event) => {
    setOperator(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    let result;
  
    switch (operator) {
      case "+":
        result = parseInt(num1) + parseInt(num2);
        break;
      case "-":
        result = parseInt(num1) - parseInt(num2);
        break;
      case "*":
        result = parseInt(num1) * parseInt(num2);
        break;
      case "/":
        result = parseInt(num1) / parseInt(num2);
        break;
      default:
        result = "Invalid operator";
    }
    alert("The answer = " + result);
  };

  return (
    <div className="content">
      <h1>Workshop 2</h1>
      <p>
        ฟังก์ชันคำนวณเลขสองจำนวน โดยสามารถเลือก บวก ลบ คูณ หาร เมื่อกดปุ่ม
        Calculate จะแสดงผลลัพธ์ที่ถูกต้อง
      </p>
      <div className="calculator">
        <form onSubmit={handleSubmit}>
          <label>
            <input
              type="number"
              value={num1}
              onChange={handleNum1Change}
            />
          </label>
          
          <label>
            <input
              type="number"
              value={num2}
              onChange={handleNum2Change}
            />
          </label>
          <br/>
          <label>
            Select Operator<br/>
            <select value={operator} onChange={handleOperatorChange}>
              <option value="+">+</option>
              <option value="-">-</option>
              <option value="*">*</option>
              <option value="/">/</option>
            </select>
          </label>
          <br />
          <button type="submit" >
            Calculate
          </button>
        </form>
      </div>
    </div>
  );
}

export default Calculator;
