import { useState } from "react";
import "./Ws02.css"
function Ws02(){
        const [sum , total] = useState(0);
        const [num1, setnum1] = useState(0);
        const [num2, setnum2] = useState(0);
        const [op, setop] = useState("+"); 
        
        function Cal(num1,num2,op){
            num1 = parseInt(num1)
            num2 = parseInt(num2)
            if (op == "+"){
                total( num1+num2)
            }
            if (op == "-"){
                total(num1-num2)
            }
            if (op == "*"){
                total(num1*num2)
            }
            if (op == "/"){
                total(num1/num2)
            }
            console.log(sum);
        }
        return(
            <>
            <div className="container">  
                <div className="content">
                <h1 className="head">Calculator</h1>
                    <input type="number"  value = {num1} onChange = {(e)=>setnum1(e.target.value)}/>
                    <select className="op" id="symbol" value = {op} onChange = {(e)=>setop(e.target.value)}> 
                        <option value="+">+</option>
                        <option value="-">-</option>
                        <option value="*">*</option>
                        <option value="/">/</option>
                    </select>
                    <input type="number"  value = {num2} onChange = {(e)=>setnum2(e.target.value)} />
                    <button onClick={() => Cal(num1, num2, op)}>calculate</button> 
                    <h1 className="sum">{sum}</h1>
                </div>
                </div>
            </>
        )
    
}

export default Ws02