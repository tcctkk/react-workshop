import { useState } from "react"
function Cout() {
    const [count,setCount]=useState(0)
    function addCount(){
        console.log(count)
        setCount(count+1)
    } 
  return (
    <div>
      <h1>{count}</h1>
      <button onClick={addCount}>เพิ่มค่า</button>
    </div>
  );
}

export default Cout;