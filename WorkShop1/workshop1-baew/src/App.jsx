import { useState } from "react";
import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
// import "./workshop/ws1/Workshop1.css";
// import Workshop1 from "./workshop/ws1/Workshop1";
import "./workshop/ws2/Workshop2.css";
import Workshop2 from "./workshop/ws2/Workshop2";

function App() {

  return (
    <>
      <Workshop2 />
    </>
  );
}

export default App;
