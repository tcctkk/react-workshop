import { Routes, Route, BrowserRouter} from "react-router-dom";
import Navbar from "./components/Navbar.jsx";
import User from "./components/User.jsx";
import UserCreate from "./components/UserCreate.jsx";
import UserUpdate from "./components/UserUpdate.jsx";

import "./App.css";

function App() {
  return (
    <div>
      <BrowserRouter>
      <Navbar />
      <Routes>
        <Route path="/" element={<User />} />
        <Route path="create" element={<UserCreate />} />
        <Route path="update/:id" element={<UserUpdate />} />
      </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
