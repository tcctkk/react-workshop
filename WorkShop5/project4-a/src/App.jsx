import "./App.css";
import Navbar from "./components/Navbar";
import Users from "./components/Users";
import { Route, Routes, BrowserRouter } from "react-router-dom";
import UserCreate from "./components/UserCreate";
import EditUser from "./components/EditUser";

function App() {
  return (
    <BrowserRouter>
      <Navbar />
      <Routes>
        <Route path="/" element={<Users />} />
        <Route path="/userCreate" element={<UserCreate />} />
        <Route path="/updateUser/:id" element={<EditUser />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
