import * as React from 'react';
import Link from "@mui/material/Link"
import Grid from '@mui/material/Unstable_Grid2';
import Box from '@mui/material/Box';
import Buttonbiology from '../public/picture/Buttonbiology.png'
import Buttonchemisty from '../public/picture/Buttonchemisty.png'
import Buttonenglish from '../public/picture/Buttonenglish.png'
import Buttongeography from '../public/picture/Buttongeography.png'
import Buttonmaths from '../public/picture/Buttonmaths.png'
import Buttonphysics from '../public/picture/Buttonphysics.png'
import './subjectItem.css'



export default function SubjectItem(){
    return(
        <div className='containerSubjectItem'>
        <div className='topic'>
            <h2>My subjects</h2>
            <Link className='LinkViex'>View All</Link>
        </div>
        
        <Box className="Boxitem">
        <Grid container spacing={3}>
        <Grid xs ={4}>
            <Link className='Button-item' sx={{textDecoration:'none' }}><img  src={Buttonbiology} alt="" /><p >Biology</p> </Link>
        </Grid>
        <Grid xs={4}>
            <Link className='Button-item' sx={{textDecoration:'none'}}><img src={Buttonchemisty} alt="" /> <p>Chemisty</p> </Link>
        </Grid>
        <Grid xs={4}>
            <Link className='Button-item' sx={{textDecoration:'none'}}><img src={Buttonphysics} alt="" /><p>Physics</p> </Link>
        </Grid>
        <Grid xs={4}>
        <Link className='Button-item' sx={{textDecoration:'none'}}><img src={Buttonenglish} alt="" /><p>English</p> </Link>

        </Grid>
        <Grid xs={4}>
        <Link className='Button-item' sx={{textDecoration:'none'}}><img src={Buttonmaths} alt="" /><p>Maths</p> </Link>
        </Grid>
        <Grid xs={4}>
        <Link className='Button-item' sx={{textDecoration:'none'}}><img src={Buttongeography} alt="" /> <p>Geography</p> </Link>
        </Grid>
        </Grid>   
        </Box>
        </div>
    )
}