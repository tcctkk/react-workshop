import React, { useEffect, useState } from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import { styled } from "@mui/material/styles";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import ButtonBase from "@mui/material/ButtonBase";
import CreditCardIcon from "@mui/icons-material/CreditCard";
import WalletIcon from "@mui/icons-material/Wallet";
import SendIcon from "@mui/icons-material/Send";
import BorderAllIcon from "@mui/icons-material/BorderAll";
import LayersRoundedIcon from "@mui/icons-material/LayersRounded";
import CreditScoreRoundedIcon from "@mui/icons-material/CreditScoreRounded";
import NoEncryptionRoundedIcon from "@mui/icons-material/NoEncryptionRounded";
import { useTheme } from "@mui/material/styles";

const Img = styled("img")({
  margin: "auto",
  display: "block",
  maxWidth: "100%",
  maxHeight: "100%",
});

export default function HomeDesign({ user, setUser }) {
  const theme = useTheme();
  const [users, setUsers] = useState([]);
  useEffect(() => {
    fetch("https://www.melivecode.com/api/users")
      .then((res) => res.json())
      .then((result) => setUsers(result))
      .catch((err) => console.log("Error", err));
  }, []);

  const showValue = (data) => {
    if (data === 0) {
      return "Card Center";
    } else if (data === 1) {
      return "E-Wallet";
    } else if (data === 2) {
      return "Send it";
    } else {
      return "Other";
    }
  };
  const setShowIcon = (data) => {
    if (data === 0) {
      return <CreditCardIcon sx={{ height: "3rem", width: "3rem" }} />;
    } else if (data === 1) {
      return <WalletIcon sx={{ height: "3rem", width: "3rem" }} />;
    } else if (data === 2) {
      return <SendIcon sx={{ height: "3rem", width: "3rem" }} />;
    } else {
      return <BorderAllIcon sx={{ height: "3rem", width: "3rem" }} />;
    }
  };

  return (
    <React.Fragment>
      <CssBaseline />
      <Container
        sx={{
          // p: 1,
          backgroundColor: "#f5f6fb",
          px: 3,
          mt: -13,
          [theme.breakpoints.up("sm")]: {
            borderRadius: "0",
            mt: 0,
            maxWidth: "lg",
            p: 0,
          },
        }}
      >
        <Box
          sx={{
            bgcolor: "white",
            width: "100%",
            // top: "-70px",
            height: "50vw",
            flexGrow: 1,
            borderRadius: 2,
            maxWidth: "100%",
            margin: "auto",
            position: "relative",
            zIndex: 2,
            [theme.breakpoints.up("sm")]: {
              position: "unset",
              zIndex: 0,
              mt: 10,
              height: "30vw",
            },
          }}
        >
          <Grid
            item
            sx={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              height: "30%",
              borderBottom: "1px solid #f5f6fb",
              p: 2.5,

              [theme.breakpoints.up("sm")]: {
                fontSize: "27px",
                height: "20%",
              },
            }}
          >
            <Typography
              variant="subtitle1"
              component="div"
              sx={{
                fontSize: "1.7rem",
                fontWeight: 700,
                color: "#52595D",
                letterSpacing: ".05rem",
                [theme.breakpoints.up("sm")]: {
                  fontSize: "27px",
                },
              }}
            >
              My total balance
            </Typography>
            <Typography
              variant="subtitle1"
              component="div"
              sx={{
                fontSize: "1.7rem",
                fontWeight: 900,
                color: "#52595D",
                [theme.breakpoints.up("sm")]: {
                  fontSize: "27px",
                },
              }}
            >
              $ 3700
            </Typography>
          </Grid>

          <Grid
            item
            xs={12}
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
              height: "60%",
              pt: 2,
              px: 1,

              [theme.breakpoints.up("sm")]: {
                height: "30%",
                px: 10,
                mt: 10,
              },
            }}
          >
            <Grid container justifyContent="space-between">
              <Box
                sx={{
                  width: "12rem",
                  height: "100%",
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  justifyContent: "space-around",
                  [theme.breakpoints.up("sm")]: {
                    width: "192px",
                  },
                }}
              >
                <Paper
                  sx={{
                    height: "7rem",
                    width: "7rem",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    boxShadow: 0,

                    bgcolor: "#f5f6fb",
                    borderRadius: "50%",
                    [theme.breakpoints.up("sm")]: {
                      height: "112px",
                      width: "112px",
                    },
                  }}
                >
                  <LayersRoundedIcon sx={{ height: "3rem", width: "3rem" }} />
                </Paper>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                    pt: 1,
                    alignItems: "center",
                  }}
                >
                  <Typography
                    variant="h6"
                    noWrap
                    component="a"
                    href="/"
                    sx={{
                      color: "#52595D",
                      letterSpacing: ".1rem",
                      textDecoration: "none",
                      fontSize: "1.4rem",
                      fontWeight: 600,
                      [theme.breakpoints.up("sm")]: {
                        fontSize: "22px",
                      },
                    }}
                  >
                    Active Balance
                  </Typography>
                  <Typography
                    variant="h6"
                    noWrap
                    component="a"
                    href="/"
                    sx={{
                      color: "#019dd5",
                      letterSpacing: ".1rem",
                      textDecoration: "none",
                      fontSize: "1.4rem",
                      fontWeight: 600,
                      [theme.breakpoints.up("sm")]: {
                        fontSize: "22px",
                      },
                    }}
                  >
                    $ 1000
                  </Typography>
                </Box>
              </Box>
              <Box
                sx={{
                  width: "12rem",
                  height: "100%",
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  justifyContent: "space-around",
                  [theme.breakpoints.up("sm")]: {
                    width: "192px",
                  },
                }}
              >
                <Paper
                  sx={{
                    height: "7rem",
                    width: "7rem",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    boxShadow: 0,
                    bgcolor: "#f5f6fb",
                    borderRadius: "50%",
                    [theme.breakpoints.up("sm")]: {
                      height: "112px",
                      width: "112px",
                    },
                  }}
                >
                  <CreditScoreRoundedIcon
                    sx={{ height: "3rem", width: "3rem" }}
                  />
                </Paper>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                    pt: 1,
                    alignItems: "center",
                  }}
                >
                  <Typography
                    variant="h6"
                    noWrap
                    component="a"
                    href="/"
                    sx={{
                      color: "#52595D",
                      letterSpacing: ".1rem",
                      textDecoration: "none",
                      fontSize: "1.4rem",
                      fontWeight: 600,
                      [theme.breakpoints.up("sm")]: {
                        fontSize: "22px",
                      },
                    }}
                  >
                    Card
                  </Typography>
                  <Typography
                    variant="h6"
                    noWrap
                    component="a"
                    href="/"
                    sx={{
                      color: "#019dd5",
                      letterSpacing: ".1rem",
                      textDecoration: "none",
                      fontSize: "1.4rem",
                      fontWeight: 600,
                      [theme.breakpoints.up("sm")]: {
                        fontSize: "22px",
                      },
                    }}
                  >
                    $ 2000
                  </Typography>
                </Box>
              </Box>
              <Box
                sx={{
                  width: "12rem",
                  height: "100%",
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  justifyContent: "space-around",
                  [theme.breakpoints.up("sm")]: {
                    width: "192px",
                  },
                }}
              >
                <Paper
                  sx={{
                    height: "7rem",
                    width: "7rem",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    boxShadow: 0,
                    bgcolor: "#f5f6fb",
                    borderRadius: "50%",
                    [theme.breakpoints.up("sm")]: {
                      height: "112px",
                      width: "112px",
                    },
                  }}
                >
                  <NoEncryptionRoundedIcon
                    sx={{ height: "3rem", width: "3rem" }}
                  />
                </Paper>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                    pt: 1,
                    alignItems: "center",
                  }}
                >
                  <Typography
                    variant="h6"
                    noWrap
                    component="a"
                    href="/"
                    sx={{
                      color: "#52595D",
                      letterSpacing: ".1rem",
                      textDecoration: "none",
                      fontSize: "1.4rem",
                      fontWeight: 600,
                      [theme.breakpoints.up("sm")]: {
                        fontSize: "22px",
                      },
                    }}
                  >
                    Save it
                  </Typography>
                  <Typography
                    variant="h6"
                    noWrap
                    component="a"
                    href="/"
                    sx={{
                      color: "#019dd5",
                      letterSpacing: ".1rem",
                      textDecoration: "none",
                      fontSize: "1.4rem",
                      fontWeight: 600,
                      [theme.breakpoints.up("sm")]: {
                        fontSize: "22px",
                      },
                    }}
                  >
                    $ 1700
                  </Typography>
                </Box>
              </Box>
            </Grid>
          </Grid>
        </Box>
      </Container>
      <Container
        sx={{
          p: 3,
          flexGrow: 1,
          [theme.breakpoints.up("sm")]: {
            maxWidth: "lg",
          },
        }}
      >
        <Box
          sx={{
            bgcolor: "inherit",
            height: "100%",
            [theme.breakpoints.up("sm")]: {
              padding: 8,
            },
          }}
        >
          <Grid
            sx={{
              flexGrow: 1,
              pt: 2,
              [theme.breakpoints.up("sm")]: {},
            }}
            container
          >
            <Grid item xs={12}>
              <Grid
                container
                justifyContent="space-between"
                sx={{
                  flexGrow: 1,
                  height: "12rem",
                  [theme.breakpoints.up("sm")]: {
                    height: "200px",
                  },
                }}
              >
                {[0, 1, 2, 3].map((value) => (
                  <Grid
                    key={value}
                    item
                    sx={{
                      display: "flex",
                      flexDirection: "column",
                      alignItems: "center",
                      justifyContent: "space-between",
                    }}
                  >
                    <Paper
                      sx={{
                        height: "7rem",
                        width: "7rem",
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        boxShadow: 0,
                        borderRadius: "8px",
                        [theme.breakpoints.up("sm")]: {
                          height: "140px",
                          width: "140px",
                        },
                      }}
                    >
                      {setShowIcon(value)}
                    </Paper>
                    <Typography
                      variant="h6"
                      noWrap
                      component="a"
                      href="/"
                      sx={{
                        color: "#52595D",
                        letterSpacing: ".1rem",
                        textDecoration: "none",
                        fontSize: "1.4rem",
                        fontWeight: 600,
                        [theme.breakpoints.up("sm")]: {
                          fontSize: "22px",
                        },
                      }}
                    >
                      {showValue(value)}
                    </Typography>
                  </Grid>
                ))}
              </Grid>
            </Grid>
          </Grid>
          <Box
            sx={{
              flexGrow: 1,
              height: "6.5rem",
              display: "flex",
              alignItems: "center",
              py: 4,
              [theme.breakpoints.up("sm")]: {
                height: "140px",
              },
            }}
          >
            <Typography
              variant="h6"
              noWrap
              component="a"
              href="/"
              sx={{
                color: "#52595D",
                textDecoration: "none",
                fontSize: "1.8rem",
                letterSpacing: ".1rem",
                fontWeight: 600,
                [theme.breakpoints.up("sm")]: {
                  fontSize: "28px",
                },
              }}
            >
              In & Out transactions
            </Typography>
          </Box>
          {users.map((data) => (
            <Paper
              sx={{
                margin: "auto",
                flexGrow: 1,
                color: "black",
                display: "flex",
                justifyContent: "center",
                mb: 2,
                borderRadius: "10px",
                boxShadow: 0,
              }}
              key={data.id}
            >
              <Box
                sx={{
                  flexGrow: 1,
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                  p: 1,
                  borderRadius: "10px",
                  [theme.breakpoints.up("sm")]: {
                    p: 3,
                  },
                }}
              >
                <Grid item>
                  <ButtonBase
                    sx={{
                      width: 50,
                      height: 50,
                      [theme.breakpoints.up("sm")]: { width: 100, height: 100 },
                    }}
                  >
                    <Img alt={data.fname} src={data.avatar} />
                  </ButtonBase>
                </Grid>
                <Grid item xs={7} sm container>
                  <Grid
                    item
                    xs
                    container
                    sx={{
                      [theme.breakpoints.up("sm")]: { ml: 4 },
                    }}
                  >
                    <Grid item xs>
                      <Typography
                        gutterBottom
                        variant="subtitle1"
                        component="div"
                        sx={{
                          fontSize: "1.7rem",
                          fontWeight: 700,
                          color: "#52595D",
                        }}
                      >
                        {data.fname} {data.lname}
                      </Typography>
                      <Typography
                        variant="body2"
                        gutterBottom
                        sx={{ fontSize: "1.2rem", color: "gray" }}
                      >
                        {data.username}
                      </Typography>
                    </Grid>
                  </Grid>
                </Grid>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "space-around",
                    alignItems: "center",
                    height: "100%",
                  }}
                >
                  <Grid item>
                    <Typography
                      variant="subtitle1"
                      component="div"
                      sx={{
                        fontSize: "1.7rem",
                        fontWeight: 700,
                        color: "#34282C",
                      }}
                    >
                      $19.00
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Typography
                      sx={{
                        fontWeight: 700,
                        cursor: "pointer",
                        fontSize: "1.4rem",
                        color: data.id % 2 === 0 ? "lightgreen" : "orange",
                      }}
                      variant="body2"
                    >
                      {data.id % 2 === 0 ? "sent" : "Received"}
                    </Typography>
                  </Grid>
                </Box>
              </Box>
            </Paper>
          ))}
        </Box>
      </Container>
    </React.Fragment>
  );
}
{
}
