import React, { useEffect } from "react";
import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomeDesign from "./components/HomeDesign";
import InAndOut from "./components/InAndOut";
import NavBar from "./components/NavBar";
import BottomBar from "./components/BottomBar";
import Box from "@mui/material/Box";
import SearchIcon from "@mui/icons-material/Search";
import NotificationsOutlinedIcon from "@mui/icons-material/NotificationsOutlined";
import Avatar from "@mui/material/Avatar";
import SendIcon from "@mui/icons-material/Send";
import IconButton from "@mui/material/IconButton";
import { useTheme } from "@mui/material/styles";

function App() {
  const theme = useTheme();
  const [user, setUser] = React.useState("");
  const [users, setUsers] = React.useState([]);

  useEffect(() => {
    fetch("https://www.melivecode.com/api/users")
      .then((res) => res.json())
      .then((result) => setUsers(result))
      .catch((err) => console.log("Error ", err));
  }, []);

  useEffect(() => {
    fetch("https://www.melivecode.com/api/users/2")
      .then((res) => res.json())
      .then((result) => setUser(result.user))
      .catch((err) => console.log("Error ", err));
  }, []);
  return (
    <BrowserRouter>
      <Routes>
        <Route
          path="/"
          element={
            <>
              <NavBar
                name="Hey Sulton,"
                phrase="what will you do today?"
                icon={
                  <NotificationsOutlinedIcon
                    sx={{
                      mr: 1.6,
                      width: "3.5rem",
                      height: "3.5rem",
                      transform: "rotate(-30deg)",
                      [theme.breakpoints.up("sm")]: {
                        height: "56px",
                        width: "56px",
                      },
                    }}
                  />
                }
                avatarOrIcon={
                  <IconButton
                    sx={{
                      p: 0,
                      border: "3px solid white",
                      width: "5rem",
                      height: "5rem",
                      [theme.breakpoints.up("sm")]: {
                        height: "70px",
                        width: "70px",
                      },
                    }}
                  >
                    <Avatar
                      alt="Remy Sharp"
                      src={user.avatar}
                      sx={{ width: "100%", height: "100%" }}
                    />
                  </IconButton>
                }
                maxSizeNavbar="60vw"
              />
              <HomeDesign user={user} setUser={setUser} />
            </>
          }
        />
        <Route
          path="/inAndOut"
          element={
            <>
              <NavBar
                user={user}
                setUser={setUser}
                name="In & Out"
                icon={
                  <SearchIcon
                    sx={{
                      mr: 1.6,
                      width: "3.5rem",
                      height: "3.5rem",
                      transform: "rotate(0deg)",
                      [theme.breakpoints.up("sm")]: {
                        height: "56px",
                        width: "56px",
                      },
                    }}
                  />
                }
                avatarOrIcon={
                  <SendIcon
                    sx={{
                      width: "3.5rem",
                      height: "3.5rem",
                      transform: "rotate(0deg)",
                      [theme.breakpoints.up("sm")]: {
                        height: "70px",
                        width: "70px",
                      },
                    }}
                  />
                }
                maxSizeNavbar="50vw"
              />
              <InAndOut users={users} />
            </>
          }
        />
      </Routes>
      <BottomBar />
    </BrowserRouter>
  );
}

export default App;
