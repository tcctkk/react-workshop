import * as React from "react";
import Box from "@mui/material/Box";
import BottomNavigation from "@mui/material/BottomNavigation";
import BottomNavigationAction from "@mui/material/BottomNavigationAction";
import RestoreIcon from "@mui/icons-material/Restore";
import FavoriteIcon from "@mui/icons-material/Favorite";
import LocationOnIcon from "@mui/icons-material/LocationOn";

import SettingsIcon from "@mui/icons-material/Settings";
import Paper from "@mui/material/Paper";

import BookmarksIcon from "@mui/icons-material/Bookmarks";
import ForumIcon from "@mui/icons-material/Forum";
import ExploreIcon from "@mui/icons-material/Explore";
import AppsIcon from "@mui/icons-material/Apps";
import CssBaseline from "@mui/material/CssBaseline";

export default function BottomNavigate() {
  const [value, setValue] = React.useState(0);

  return (
    <Paper
      sx={{
        position: "fixed",
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: "##F9FAFF",
      }}
      elevation={3}
    >
      <BottomNavigation
        showLabels
        value={value}
        sx={{ width: "94.3%", backgroundColor: "#F9FAFF", margin: "auto",minWidth:"75px"}}
        onChange={(event, newValue) => {
          setValue(newValue);
        }}
      >
        <BottomNavigationAction
          color="secondary"
          label="Applications"
          icon={<AppsIcon />}
        />
        <BottomNavigationAction label="Discover" icon={<ExploreIcon />} />
        <BottomNavigationAction label="Community" icon={<ForumIcon />} />
        <BottomNavigationAction label="Bookmarks" icon={<BookmarksIcon />} />
        <BottomNavigationAction label="Setting" icon={<SettingsIcon />} />
      </BottomNavigation>
    </Paper>
  );
}
