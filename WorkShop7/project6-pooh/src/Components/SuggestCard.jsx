import * as React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import FilterList from "@mui/icons-material/FilterList";
import Grid from "@mui/material/Grid";
import "./CC.css";
import CardLink from "./CardLink";


export default function SuggestCard() {

  // const lo="Villa Gaanok Komong" ,ra="4.9", pr="79", im="https://images.unsplash.com/photo-1551963831-b3b1ca40c98e"

  const items = [{
    lo: "Villa Gaanok Komang",
    ra: "4.9",
    pr: "78",
    im: "https://q-xx.bstatic.com/xdata/images/hotel/max1024x768/314234927.jpg?k=e8d0ff333645000b3345bf3c924ebdba3547cd73c362881b16049f5fe5d19701&o="
},
{
    lo: "Villa Parttes Ubud",
    ra: "4.8",
    pr: "112",
    im: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR87BbUcKwYHZcRGOnyovgU7c0xxCNnfePVbQ&usqp=CAU"
},
{
    lo: "Banny's Apartments",
    ra: "4.9",
    pr: "89",
    im: "https://images.posarellivillas.com/property_posarelli/96037/xwide/dsc-5347-easyhdr.jpg"
},
{
    lo: "Villa Banoon Kuta Utara",
    ra: "4.9",
    pr: "119",
    im: "https://www.villaabiente.com/resources/abiente/headers/mobile/xVilla,P20Abiente,P20-,P20Magnificent,P20exterior,P20villa.jpg.pagespeed.ic.e6bNABZFo5.jpg"
},
]

  return (
    <React.Fragment>
      <CssBaseline />
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          justifyItems: "space-evenly",
          padding: "20px",
          paddingTop: "40px",
          paddingBottom: "10px",
        }}
      >
        <Typography
          variant="h1"
          gutterBottom
          sx={{
            color: "#4E4C66",
            fontSize: "18px",
            fontWeight: "bold",
          }}
        >
          Popular stays
        </Typography>

        <FilterList
          sx={{
            backgroundColor: "white",
            borderRadius: "0.5rem",
            width: "30px",
            height: "30px",
            padding: "2px",
            boxShadow: "2px 2px 5px #888888",
            alignItems: "flex-start",
            color:"dark",
          }}
        />
      </Box>

      <Box
        sx={{
          flexGrow: 1,
          display: "flex",
          margin: "4%",
          marginTop:"10px",
          minWidth: "100px",
          flexWrap: "wrap",
        }}
      >
        <Grid container>
        {items.map((item)=>(
          <Grid
          key={item.lo}
          item
          xs={6}
          sm={4}
          md={3}
          sx={{
            maxWidth: "auto",
            maxHeight: "auto",
            alignItems: "flex-start",
            padding: "5px",
          }}
        >
          <CardLink lo={item.lo} ra={item.ra} pr={item.pr} im={item.im}/>
        </Grid>
      ))}
        </Grid>
      </Box>
    </React.Fragment>
  );
}
