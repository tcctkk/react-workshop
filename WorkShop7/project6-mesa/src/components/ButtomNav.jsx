import * as React from "react";
import { Link } from "react-router-dom";
import CssBaseline from "@mui/material/CssBaseline";
import Paper from "@mui/material/Paper";
import BottomNavigation from "@mui/material/BottomNavigation";
import BottomNavigationAction from "@mui/material/BottomNavigationAction";
import FeedOutlinedIcon from "@mui/icons-material/FeedOutlined";
import AttachMoneyOutlinedIcon from "@mui/icons-material/AttachMoneyOutlined";
import SyncAltOutlinedIcon from "@mui/icons-material/SyncAltOutlined";
import ReplayOutlinedIcon from "@mui/icons-material/ReplayOutlined";
import SortOutlinedIcon from "@mui/icons-material/SortOutlined";

export default function ButtomNav() {
  const [value, setValue] = React.useState("feed");

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <React.Fragment>
      <CssBaseline />
      <Paper
        sx={{
          position: "fixed",
          bottom: 0,
          left: 0,
          right: 0,
          borderTopLeftRadius: 20,
          borderTopRightRadius: 20,
          zIndex: 1,
        }}
        elevation={5}
      >
        <BottomNavigation
          sx={{
            width: "100%",
            borderTopLeftRadius: 20,
            borderTopRightRadius: 20,
          }}
          value={value}
          onChange={handleChange}
        >
          <BottomNavigationAction
            label="Feed"
            value="feed"
            icon={<FeedOutlinedIcon />}
            component={Link}
            to="/"
            selected={location.pathname === "/"}
          />
          <BottomNavigationAction
            label="Products"
            value="products"
            icon={<AttachMoneyOutlinedIcon />}
            component={Link}
            to="/products"
            selected={location.pathname === "/products"}
          />
          <BottomNavigationAction
            label="Exchange"
            value="exchange"
            icon={<SyncAltOutlinedIcon />}
          />
          <BottomNavigationAction
            label="Return"
            value="return"
            icon={<ReplayOutlinedIcon />}
          />
          <BottomNavigationAction
            label="List"
            value="list"
            icon={<SortOutlinedIcon />}
          />
        </BottomNavigation>
      </Paper>
    </React.Fragment>
  );
}
