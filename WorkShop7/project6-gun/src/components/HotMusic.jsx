import { Box, IconButton, Typography } from "@mui/material";
import "./HotMusic.css";
import PlayCircleIcon from "@mui/icons-material/PlayCircle";
export default function HotMusic() {
  return (
    <>
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <Typography sx={{ fontSize: "20px", fontWeight: "bold" }}>
          Hot Music
        </Typography>
        <Typography sx={{ fontSize: "12px" }}>See all</Typography>
      </Box>
      <Box
        sx={{
          marginTop: 2,
          display: "flex",
          padding: " 0px 20px 20px 0px",
        }}
        className="container-hotmusic"
      >
        <Box
          sx={{
            bgcolor: "#fe0",
            width: "140px",
            height: "120px",
            borderRadius: "15px",
            marginRight: "10px",
          }}
        >
          <Box
            sx={{
              width: "140px",
              height: "120px",
              backgroundImage:
                "url(https://www.usmagazine.com/wp-content/uploads/2020/07/Jermaine-Cole-J-Cole-Confirms-He-Has-2-Kids.jpg?quality=86&strip=all)",
              backgroundSize: "cover",
              backgroundRepeat: "no-repeat",
              backgroundPosition: "center",
              display: "flex",
              alignItems: "end",
              borderRadius: "15px",
            }}
          >
            <Box
              sx={{
                bgcolor: "#fff",
                width: "100%",
                height: "60px",
                borderRadius: "15px",
                transform: "translate(0,25%)",
                boxShadow: " rgba(0, 0, 0, 0.16) 0px 1px 4px",
              }}
            >
              <Box
                sx={{
                  p: 1,
                  display: "flex",
                  alignItems: "center",
                  height: "100%",
                  justifyContent: "space-between",
                }}
              >
                <Box>
                  <Typography sx={{ fontSize: "11px", fontWeight: "bold" }}>
                    Middle Child
                  </Typography>
                  <Typography sx={{ fontSize: "11px" }}>By J.Cole</Typography>
                </Box>
                <IconButton
                  sx={{
                    "--IconButton-size": "100px",
                  }}
                >
                  <PlayCircleIcon sx={{ color: "rgb(141, 67, 214)" , width:"25px"}} />
                </IconButton>
              </Box>
            </Box>
          </Box>
        </Box>



        <Box
          sx={{
            bgcolor: "#fe0",
            width: "140px",
            height: "120px",
            borderRadius: "15px",
            marginRight: "10px",
          }}
        >
          <Box
            sx={{
              width: "140px",
              height: "120px",
              backgroundImage:
                "url(https://www.usmagazine.com/wp-content/uploads/2020/07/Jermaine-Cole-J-Cole-Confirms-He-Has-2-Kids.jpg?quality=86&strip=all)",
              backgroundSize: "cover",
              backgroundRepeat: "no-repeat",
              backgroundPosition: "center",
              display: "flex",
              alignItems: "end",
              borderRadius: "15px",
            }}
          >
            <Box
              sx={{
                bgcolor: "#fff",
                width: "100%",
                height: "60px",
                borderRadius: "15px",
                transform: "translate(0,25%)",
                boxShadow: " rgba(0, 0, 0, 0.16) 0px 1px 4px",
              }}
            >
              <Box
                sx={{
                  p: 1,
                  display: "flex",
                  alignItems: "center",
                  height: "100%",
                  justifyContent: "space-between",
                }}
              >
                <Box>
                  <Typography sx={{ fontSize: "11px", fontWeight: "bold" }}>
                    Middle Child
                  </Typography>
                  <Typography sx={{ fontSize: "11px" }}>By J.Cole</Typography>
                </Box>
                <IconButton
                  sx={{
                    "--IconButton-size": "100px",
                  }}
                >
                  <PlayCircleIcon sx={{ color: "rgb(141, 67, 214)" , width:"25px"}} />
                </IconButton>
              </Box>
            </Box>
          </Box>
        </Box>


        <Box
          sx={{
            bgcolor: "#fe0",
            width: "140px",
            height: "120px",
            borderRadius: "15px",
            marginRight: "10px",
          }}
        >
          <Box
            sx={{
              width: "140px",
              height: "120px",
              backgroundImage:
                "url(https://www.usmagazine.com/wp-content/uploads/2020/07/Jermaine-Cole-J-Cole-Confirms-He-Has-2-Kids.jpg?quality=86&strip=all)",
              backgroundSize: "cover",
              backgroundRepeat: "no-repeat",
              backgroundPosition: "center",
              display: "flex",
              alignItems: "end",
              borderRadius: "15px",
            }}
          >
            <Box
              sx={{
                bgcolor: "#fff",
                width: "100%",
                height: "60px",
                borderRadius: "15px",
                transform: "translate(0,25%)",
                boxShadow: " rgba(0, 0, 0, 0.16) 0px 1px 4px",
              }}
            >
              <Box
                sx={{
                  p: 1,
                  display: "flex",
                  alignItems: "center",
                  height: "100%",
                  justifyContent: "space-between",
                }}
              >
                <Box>
                  <Typography sx={{ fontSize: "11px", fontWeight: "bold" }}>
                    Middle Child
                  </Typography>
                  <Typography sx={{ fontSize: "11px" }}>By J.Cole</Typography>
                </Box>
                <IconButton
                  sx={{
                    "--IconButton-size": "100px",
                  }}
                >
                  <PlayCircleIcon sx={{ color: "rgb(141, 67, 214)" , width:"25px"}} />
                </IconButton>
              </Box>
            </Box>
          </Box>
        </Box>

        <Box
          sx={{
            bgcolor: "#fe0",
            width: "140px",
            height: "120px",
            borderRadius: "15px",
            marginRight: "10px",
          }}
        >
          <Box
            sx={{
              width: "140px",
              height: "120px",
              backgroundImage:
                "url(https://www.usmagazine.com/wp-content/uploads/2020/07/Jermaine-Cole-J-Cole-Confirms-He-Has-2-Kids.jpg?quality=86&strip=all)",
              backgroundSize: "cover",
              backgroundRepeat: "no-repeat",
              backgroundPosition: "center",
              display: "flex",
              alignItems: "end",
              borderRadius: "15px",
            }}
          >
            <Box
              sx={{
                bgcolor: "#fff",
                width: "100%",
                height: "60px",
                borderRadius: "15px",
                transform: "translate(0,25%)",
                boxShadow: " rgba(0, 0, 0, 0.16) 0px 1px 4px",
              }}
            >
              <Box
                sx={{
                  p: 1,
                  display: "flex",
                  alignItems: "center",
                  height: "100%",
                  justifyContent: "space-between",
                }}
              >
                <Box>
                  <Typography sx={{ fontSize: "11px", fontWeight: "bold" }}>
                    Middle Child
                  </Typography>
                  <Typography sx={{ fontSize: "11px" }}>By J.Cole</Typography>
                </Box>
                <IconButton
                  sx={{
                    "--IconButton-size": "100px",
                  }}
                >
                  <PlayCircleIcon sx={{ color: "rgb(141, 67, 214)" , width:"25px"}} />
                </IconButton>
              </Box>
            </Box>
          </Box>
        </Box>

        <Box
          sx={{
            bgcolor: "#fe0",
            width: "140px",
            height: "120px",
            borderRadius: "15px",
            marginRight: "10px",
          }}
        >
          <Box
            sx={{
              width: "140px",
              height: "120px",
              backgroundImage:
                "url(https://www.usmagazine.com/wp-content/uploads/2020/07/Jermaine-Cole-J-Cole-Confirms-He-Has-2-Kids.jpg?quality=86&strip=all)",
              backgroundSize: "cover",
              backgroundRepeat: "no-repeat",
              backgroundPosition: "center",
              display: "flex",
              alignItems: "end",
              borderRadius: "15px",
            }}
          >
            <Box
              sx={{
                bgcolor: "#fff",
                width: "100%",
                height: "60px",
                borderRadius: "15px",
                transform: "translate(0,25%)",
                boxShadow: " rgba(0, 0, 0, 0.16) 0px 1px 4px",
              }}
            >
              <Box
                sx={{
                  p: 1,
                  display: "flex",
                  alignItems: "center",
                  height: "100%",
                  justifyContent: "space-between",
                }}
              >
                <Box>
                  <Typography sx={{ fontSize: "11px", fontWeight: "bold" }}>
                    Middle Child
                  </Typography>
                  <Typography sx={{ fontSize: "11px" }}>By J.Cole</Typography>
                </Box>
                <IconButton
                  sx={{
                    "--IconButton-size": "100px",
                  }}
                >
                  <PlayCircleIcon sx={{ color: "rgb(141, 67, 214)" , width:"25px"}} />
                </IconButton>
              </Box>
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  );
}
